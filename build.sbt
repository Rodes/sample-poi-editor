name := "poiweb"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.mongodb" % "mongo-java-driver" % "2.12.4"
)     

play.Project.playScalaSettings

sources in (Compile,doc) := Seq.empty

publishArtifact in (Compile, packageDoc) := false

publishArtifact in (Compile, packageSrc) := false